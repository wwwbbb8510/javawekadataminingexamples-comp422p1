import com422.conv.ConvMask;
import com422.filter.SpatialFilters;
import com422.pgm.PgmParser;

public class NoiseCancellation {
    /**
     * input file path
     */
    private static final String INPUT_FILE = "project1-images/1.2/ckt-board-saltpep.pgm";
    /**
     * output file path of median filter
     */
    private static final String OUTPUT_FILE_MEDIAN_FILTER = "project1-images/1.2/ckt-board-saltpep-output-median.pgm";
    /**
     * output file path of mean filter
     */
    private static final String OUTPUT_FILE_MEAN_FILTER = "project1-images/1.2/ckt-board-saltpep-output-mean.pgm";

    public static void main(String[] args) {
        String inputFile = INPUT_FILE;
        String outputFileMedian = OUTPUT_FILE_MEDIAN_FILTER;
        String outputFileMean = OUTPUT_FILE_MEAN_FILTER;
        if(args.length == 1){
            String resourceFolder = args[0];
            inputFile = resourceFolder + inputFile;
            outputFileMedian = resourceFolder + outputFileMedian;
            outputFileMean = resourceFolder + outputFileMean;
        }

        //parse the image to raw data
        PgmParser pp = new PgmParser(inputFile);
        int[][] rawData = pp.getRawData();

        //perform median filter
        int[][] filteredMedian =  SpatialFilters.medianFilter(rawData, 0);
        pp.outputPgm(filteredMedian, outputFileMedian);

        //perform mean filter
        ConvMask cm = new ConvMask();
        int[][] filteredDataMean =  cm.meanOperator(rawData, null);
        pp.outputPgm(filteredDataMean, outputFileMean);
    }
}
