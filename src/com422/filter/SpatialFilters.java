package com422.filter;

import com422.utils.ArrayUtils;

/**
 * Spatial filters
 */
public class SpatialFilters {
    public static final int MEDIAN_FILTER_WINDOW_SIZE_DEFAULT = 3;

    /**
     * mean filter
     * @param rawData   image raw data
     * @param windowSize    window size
     * @return  filtered raw data of the image
     */
    public static int[][] medianFilter(int[][] rawData, int windowSize){
        int[][] filteredRawData = ArrayUtils.clone2DArray(rawData);

        //use default window size if windowSize is null
        if(windowSize <= 0){
            windowSize = MEDIAN_FILTER_WINDOW_SIZE_DEFAULT;
        }

        //check image length
        int imgHeight = rawData.length;
        int imgWidth = rawData[0].length;

        //apply median filter
        for(int row=0; row < (imgHeight - windowSize); row++){
            for(int col=0; col < (imgWidth - windowSize); col++){
                int[][] targetSelection = ArrayUtils.slice2DArray(rawData, col, windowSize, row, windowSize);
                int[] target1D = ArrayUtils.convert2DTo1D(targetSelection);
                int median = ArrayUtils.calculateMedian(target1D);
                filteredRawData[row+(windowSize - 1)/2][col+(windowSize - 1)/2] = median;
            }
        }
        return filteredRawData;
    }
}
