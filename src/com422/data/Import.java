package com422.data;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Import {
    /**
     * load data from file
     * @param filePath  path
     * @return loaded data in ArrayList
     */
    public static ArrayList<ArrayList<Float>> LoadFromFile(String filePath){
        ArrayList<ArrayList<Float>> loadedData = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new File(filePath));
            while(sc.hasNextLine()){
                ArrayList<Float> rowArray = new ArrayList<>();
                String lineString = sc.nextLine();
                Scanner lsc = new Scanner(lineString);
                while (lsc.hasNext()){
                    if(lsc.hasNextFloat()){
                        rowArray.add(lsc.nextFloat());
                    }else{
                        lsc.next();
                    }
                }
                lsc.close();
                loadedData.add(rowArray);
            }
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return loadedData;
    }

    /**
     * load data from file with file stream
     * @param filePath  path
     * @return  loaded data in ArrayList
     */
    public static ArrayList<ArrayList<Float>> loadFromPgmFile(String filePath){
        ArrayList<ArrayList<Float>> loadedData = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            InputStream is = new FileInputStream(filePath);
            int i;
            while((i=is.read()) != -1){
                bos.write(i);
            }
            is.close();
            byte[] bytesFromFile = bos.toByteArray();
            bos.close();
            loadedData = parseP5Bytes(bytesFromFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return loadedData;
    }

    /**
     * parse P5 bytes to raw data
     * @param bytesP5   bytes array
     * @return
     */
    protected static ArrayList<ArrayList<Float>> parseP5Bytes(byte[] bytesP5){
        ArrayList<ArrayList<Float>> loadedData = new ArrayList<>();
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytesP5);
            ByteOutputStream bosImgType = new ByteOutputStream();//image type
            ByteOutputStream bosImgSize = new ByteOutputStream();//image size
            ByteOutputStream bosMaxPixValue = new ByteOutputStream();//max pixel value
            ByteOutputStream bosData = new ByteOutputStream();//image data

            //parse all data from file
            int lineNum = 1;
            int i;
            while((i=bis.read()) != -1){
                if(i == 10){//line break
                    lineNum++;
                    continue;
                }
                if(lineNum == 1){
                    bosImgType.write(i);
                }else if(lineNum == 2){
                    bosImgSize.write(i);
                }else if(lineNum == 3){
                    bosMaxPixValue.write(i);
                }else if(lineNum == 4){
                    bosData.write(i);
                }
            }

            //parse image data from
            String imgSize = bosImgSize.toString();
            String[] arrImgSize = imgSize.split(" ");
            int imgWidth = Integer.parseInt(arrImgSize[0]);
            int imgHeight = Integer.parseInt(arrImgSize[1]);
            byte[] bytesData = bosData.getBytes();
            for(int row=0; row<imgHeight; row++){
                ArrayList<Float> arrRowData = new ArrayList<>();
                for (int col=0; col<imgWidth; col++){
                    int index = row * imgWidth + col;
                    if (index < bytesData.length){
                        arrRowData.add(new Float(bytesData[index]));
                    }
                }
                loadedData.add(arrRowData);
            }
            bis.close();
            bosImgType.close();
            bosImgSize.close();
            bosMaxPixValue.close();
            bosData.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return loadedData;
    }

    /**
     * read all files in the folder and convert them to a 3D ArrayList - instance-row-column pixel
     * @param folder
     * @return
     */
    public static ArrayList<ArrayList<ArrayList<Float>>> loadAllFromDirectory(String folder){
        ArrayList<ArrayList<ArrayList<Float>>> dataset = new ArrayList<>();
        File dir = new File(folder);
        for(File file: dir.listFiles()){
            if(file.isDirectory()){
                System.out.println(file.getAbsolutePath()+" is directory");
            }else{
                System.out.println("load data from file: " + file.getAbsolutePath());
                dataset.add(loadFromPgmFile(file.getAbsolutePath()));
            }
        }
        return dataset;
    }
}
