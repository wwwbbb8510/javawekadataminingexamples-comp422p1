package com422.dm;

import com422.utils.ArrayUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FeatureExtraction {
    protected static final int WINDOW_SIZE = 10;
    /**
     * extracted features from a set of pixel data
     * @param pixelData
     * @return
     */
    public static Map<String, ArrayList> extractFeaturesFromPixels(ArrayList<ArrayList<ArrayList<Float>>> pixelData){
        //extracted feature data
        ArrayList<ArrayList<Object>> extractedFeatureData = null;
        //extracted feature names
        ArrayList<String> extractedFeatureAttributeNames = new ArrayList<>();

        //add feature names
        extractedFeatureAttributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("row-col-mean-", 38));
        extractedFeatureAttributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("row-col-sd-", 38));
        extractedFeatureAttributeNames.add("hist-32");
        extractedFeatureAttributeNames.add("hist-64");
        extractedFeatureAttributeNames.add("hist-96");
        extractedFeatureAttributeNames.add("hist-128");
        extractedFeatureAttributeNames.add("hist-160");
        extractedFeatureAttributeNames.add("hist-192");
        extractedFeatureAttributeNames.add("hist-224");
        extractedFeatureAttributeNames.add("hist-256");
        extractedFeatureAttributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("centred-average-", 9));
        extractedFeatureAttributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("slide-window", 100));

        //add feature data
        System.out.println("Extracting mean feature");
        extractedFeatureData = extractMeanBrightness(pixelData);
        System.out.println("Extracting standard deviation feature");
        extractedFeatureData = ArrayUtils.join2DArrayList(extractedFeatureData,
                extractStandardDeviation(pixelData, extractedFeatureData));
        System.out.println("Extracting hist 32, 64, 96, 128, 160, 192, 224, 256 features");
        extractedFeatureData = ArrayUtils.join2DArrayList(extractedFeatureData, extractHistgram(pixelData));
        System.out.println("Extracting centred average features");
        extractedFeatureData = ArrayUtils.join2DArrayList(extractedFeatureData, extractCentredAverage(pixelData));
        System.out.println("Extracting slide window features");
        extractedFeatureData = ArrayUtils.join2DArrayList(extractedFeatureData, extractSlideWindow(pixelData, 10));

        Map<String, ArrayList> extractedFeatures = new HashMap<>();
        extractedFeatures.put("arributeNames", extractedFeatureAttributeNames);
        extractedFeatures.put("data", extractedFeatureData);
        return extractedFeatures;
    }

    /**
     * extract slide window features
     * @param pixelData
     * @return
     */
    public static ArrayList<ArrayList<Object>> extractSlideWindow(ArrayList<ArrayList<ArrayList<Float>>> pixelData, int windowSize){
        ArrayList<ArrayList<Object>> slideWindowFeatures = new ArrayList<>();
        if (windowSize<=0){
            windowSize = WINDOW_SIZE;
        }
        for (ArrayList<ArrayList<Float>> imgInstance: pixelData){
            ArrayList<Object> colArr = new ArrayList<>();
            int startRowIndex=0, startColIndex = 0;
            for (int rowIndex=startRowIndex; rowIndex<=(imgInstance.size()-windowSize); rowIndex++){
                for (int colIndex= startColIndex; colIndex<=(imgInstance.get(0).size()-windowSize); colIndex++){
                    ArrayList<ArrayList<Float>> windowArr = ArrayUtils.slice2DArrayList(imgInstance, rowIndex, colIndex, windowSize, windowSize);
                    colArr.add(ArrayUtils.calculateMean(windowArr));
                }
            }
            slideWindowFeatures.add(colArr);
        }
        return slideWindowFeatures;
    }

    /**
     * extract centred average features
     * @param pixelData a set of image raw data
     * @return  the extracted centred average features
     */
    public static ArrayList<ArrayList<Object>> extractCentredAverage(ArrayList<ArrayList<ArrayList<Float>>> pixelData){
        ArrayList<ArrayList<Object>> centredAverageFeatures = new ArrayList<>();
        for (ArrayList<ArrayList<Float>> imgInstance: pixelData){
            ArrayList<Object> col = new ArrayList<>();
            ArrayList<ArrayList<Float>> clonedImgInstance = ArrayUtils.clone2DArrayList(imgInstance);
            for (int i=0; i<(imgInstance.size()/2); i++){
                col.add(ArrayUtils.calculateMean(clonedImgInstance));
                clonedImgInstance.remove(0);
                clonedImgInstance.remove(clonedImgInstance.size()-1);
                for (int row=0; row<clonedImgInstance.size(); row++){
                    clonedImgInstance.get(row).remove(0);
                    clonedImgInstance.get(row).remove(clonedImgInstance.get(row).size()-1);
                }
            }
            centredAverageFeatures.add(col);
        }
        return centredAverageFeatures;
    }

    /**
     * extract the mean brightness features
     * @param pixelData a set of image raw data
     * @return  the extracted mean feature
     */
    public static ArrayList<ArrayList<Object>> extractMeanBrightness(ArrayList<ArrayList<ArrayList<Float>>> pixelData){
        ArrayList<ArrayList<Object>> meanFeatures = new ArrayList<>();
        for (ArrayList<ArrayList<Float>> imgInstance: pixelData){
            ArrayList<Object> col = new ArrayList<>();
            for (ArrayList<Float> row: imgInstance){
                ArrayList<ArrayList<Float>> tmpRow = new ArrayList<>();
                tmpRow.add(row);
                col.add(ArrayUtils.calculateMean(tmpRow));
            }
            ArrayList<ArrayList<Float>> imgInstanceTranspose = ArrayUtils.transpose2DArray(imgInstance);
            for (ArrayList<Float> row: imgInstanceTranspose){
                ArrayList<ArrayList<Float>> tmpRow = new ArrayList<>();
                tmpRow.add(row);
                col.add(ArrayUtils.calculateMean(tmpRow));
            }
            meanFeatures.add(col);
        }
        return meanFeatures;
    }

    /**
     * extract standard deviation feature
     * @param pixelData a set of image raw data
     * @param arrMean   mean values of all images
     * @return  the extracted standard deviation feature
     */
    public static ArrayList<ArrayList<Object>> extractStandardDeviation(ArrayList<ArrayList<ArrayList<Float>>> pixelData, ArrayList<ArrayList<Object>> arrMean){
        ArrayList<ArrayList<Object>> deviationFeatures = new ArrayList<>();

        //calculate mean
        if(arrMean == null){
            arrMean = extractMeanBrightness(pixelData);
        }

        //calculate standard deviation
        for (int instanceIndex=0; instanceIndex < pixelData.size(); instanceIndex++){
            ArrayList<Object> col = new ArrayList<>();
            for (int rowIndex=0; rowIndex<pixelData.get(instanceIndex).size(); rowIndex++){
                ArrayList<ArrayList<Float>> tmpRow = new ArrayList<>();
                tmpRow.add(pixelData.get(instanceIndex).get(rowIndex));
                col.add(ArrayUtils.calculateStandardDeviation(tmpRow, (Float) arrMean.get(instanceIndex).get(rowIndex)));
            }
            ArrayList<ArrayList<Float>> imgInstanceTranspose = ArrayUtils.transpose2DArray(pixelData.get(instanceIndex));
            for (int rowIndex=0; rowIndex<imgInstanceTranspose.size(); rowIndex++){
                ArrayList<ArrayList<Float>> tmpRow = new ArrayList<>();
                tmpRow.add(imgInstanceTranspose.get(rowIndex));
                col.add(ArrayUtils.calculateStandardDeviation(tmpRow, (Float) arrMean.get(instanceIndex).get(pixelData.get(instanceIndex).size()+rowIndex)));
            }
            deviationFeatures.add(col);
        }
        return deviationFeatures;
    }

    /**
     * extract hist features 32, 64, 96, 128, 160, 192, 224, 256
     * @param pixelData a set of image raw data
     * @return
     */
    public static ArrayList<ArrayList<Object>> extractHistgram(ArrayList<ArrayList<ArrayList<Float>>> pixelData){
        ArrayList<ArrayList<Object>> histFeatures = new ArrayList<>();

        //calculate hist numbers
        for (ArrayList<ArrayList<Float>> imgInstance: pixelData){
            ArrayList<Object> col = new ArrayList<>();
            int[] histFrequency = {0,0,0,0,0,0,0,0};
            for (ArrayList<Float> rowData: imgInstance){
                for (Float pixVal: rowData){
                    if (pixVal < 32){
                        histFrequency[0] += 1;
                    }else if(pixVal < 64){
                        histFrequency[1] += 1;
                    }else if(pixVal < 96){
                        histFrequency[2] += 1;
                    }else if(pixVal < 128){
                        histFrequency[3] += 1;
                    }else if(pixVal < 160){
                        histFrequency[4] += 1;
                    }else if(pixVal < 192){
                        histFrequency[5] += 1;
                    }else if(pixVal < 224){
                        histFrequency[6] += 1;
                    }else{
                        histFrequency[7] += 1;
                    }
                }
            }
            col.add(histFrequency[0]);
            col.add(histFrequency[1]);
            col.add(histFrequency[2]);
            col.add(histFrequency[3]);
            col.add(histFrequency[4]);
            col.add(histFrequency[5]);
            col.add(histFrequency[6]);
            col.add(histFrequency[7]);
            histFeatures.add(col);
        }

        return histFeatures;
    }
}
