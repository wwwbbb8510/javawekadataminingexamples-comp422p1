package com422.dm;

import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.classifiers.trees.J48;
import weka.core.*;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemovePercentage;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;
import weka.gui.visualize.PlotData2D;
import weka.gui.visualize.ThresholdVisualizePanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WekaDataTransfer {

    /**
     * create weka instances from given ArrayList data
     * @param datasetName
     * @param arrAttributeNames
     * @param arrDataset
     * @return
     */
    public static Instances createNumericDataset(String datasetName, ArrayList<String> arrAttributeNames, ArrayList<ArrayList<Object>> arrDataset, ArrayList<String> labels){
        //add attributes
        ArrayList<Attribute> arrAttributes = new ArrayList<>();
        for (int i=0; i<arrAttributeNames.size(); i++){
            if(labels != null && i == (arrAttributeNames.size()-1)){
                Attribute nominal = new Attribute(arrAttributeNames.get(i), labels);
                arrAttributes.add(nominal);
            }else{
                arrAttributes.add(new Attribute(arrAttributeNames.get(i)));
            }
        }
        Instances dataset = new Instances(datasetName, arrAttributes, 0);
        //add data
        for (int i=0; i<arrDataset.size(); i++){
            double[] values = new double[arrAttributeNames.size()];
            ArrayList<Object> rowData = arrDataset.get(i);
            for (int j=0; j<rowData.size(); j++){
                values[j] = new Float(rowData.get(j).toString());
            }
            Instance rowInstance = new DenseInstance(1.0, values);
            dataset.add(rowInstance);
        }
        return dataset;
    }

    /**
     * split the dataset to two halves based on the given percentage
     * @param dataset
     * @param percentage
     * @param randSeed
     * @return
     */
    public static Map<String, Instances> splitDatasetToTrainTest(Instances dataset, double percentage, int randSeed){
        Map<String, Instances> splitedDatasets = new HashMap<>();
        if(randSeed <= 0){
            randSeed = 20;
        }
        dataset.randomize(dataset.getRandomNumberGenerator(1));
        try {
            RemovePercentage rmvp = new RemovePercentage();
            rmvp.setInputFormat(dataset);
            rmvp.setPercentage(percentage);
            Instances trainingSet = Filter.useFilter(dataset, rmvp);
            RemovePercentage rmvpInverse = new RemovePercentage();
            rmvpInverse.setInputFormat(dataset);
            rmvpInverse.setPercentage(percentage);
            rmvpInverse.setInvertSelection(true);
            Instances testSet = Filter.useFilter(dataset, rmvpInverse);
            splitedDatasets.put("train", trainingSet);
            splitedDatasets.put("test", testSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return splitedDatasets;
    }

    /**
     * generate sequential attribute names
     * @param prefix    name prefix
     * @param amount    total number
     * @return
     */
    public static ArrayList<String> generateSequentialAttributeNames(String prefix, int amount){
        ArrayList<String> attributeNames = new ArrayList<>();
        for (int i=0; i<amount; i++){
            attributeNames.add(prefix + "-" + i);
        }
        return attributeNames;
    }

    /**
     * plot ROC curve
     * @param eval
     * @param classIndex
     * @throws Exception
     */
    public static void plotROC(Evaluation eval, int classIndex) throws Exception {
        //generate plot data
        ThresholdCurve tc = new ThresholdCurve();
        Instances curve = tc.getCurve(eval.predictions(), classIndex);

        //put the plot data into a plot container
        PlotData2D plotdata = new PlotData2D(curve);
        plotdata.setPlotName(curve.relationName());
        plotdata.addInstanceNumberAttribute();

        //Add the plot container to a visualization panel
        ThresholdVisualizePanel tvp = new ThresholdVisualizePanel();
        tvp.setROCString("(Area under ROC = " + Utils.doubleToString(ThresholdCurve.getROCArea(curve),4)+")");
        tvp.setName(curve.relationName());
        tvp.addPlot(plotdata);

        //Add the visualization panel to a JFrame
        JFrame jf = new JFrame("WEKA ROC: " + tvp.getName());
        jf.setSize(500,400);
        jf.getContentPane().setLayout(new BorderLayout());
        jf.getContentPane().add(tvp, BorderLayout.CENTER);
        jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jf.setVisible(true);
    }

    /**
     * visualize J48 tree
     * @param cls
     * @param title
     * @throws Exception
     */
    public static void visualizeJ48Tree(J48 cls, String title) throws Exception {
        TreeVisualizer tv = new TreeVisualizer( null, cls.graph(), new PlaceNode2());
        title = title == null ? "Weka Classifier Tree Visualizer: J48" : title;
        JFrame jf = new JFrame(title);
        jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jf.setSize(1200, 900);
        jf.getContentPane().setLayout(new BorderLayout());
        jf.getContentPane().add(tv, BorderLayout.CENTER);
        jf.setVisible(true);
        tv.fitToScreen();
    }
}
