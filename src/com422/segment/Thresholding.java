package com422.segment;

/**
 * Thresholding segmentation
 */
public class Thresholding {
    /**
     * max value for binary threshold
     */
    private final static int MAX_VALUE = 255;

    /**
     * binary threshold
     * @param rawData   image raw data
     * @param thresholdValue    threshold value
     * @param maxValue  max value
     * @return  segmented image raw data
     */
    public static int[][] binaryThreshold(int[][] rawData, int thresholdValue, int maxValue){
        int[][] segmentedRawData = new int[rawData.length][rawData[0].length];
        if(maxValue <= 0){
            maxValue = MAX_VALUE;
        }
        for (int row=0; row<rawData.length; row++){
            for(int col=0; col<rawData[0].length; col++){
                segmentedRawData[row][col] = rawData[row][col] > thresholdValue ?
                        rawData[row][col] : 0;
            }
        }
        return segmentedRawData;
    }
}
