package com422.conv;

import com422.utils.ArrayUtils;

/**
 * convolution mask
 */
public class ConvMask {
    /**
     * default sobel vertical mask array
     */
    protected static final int[][] SOBEL_MASK_VERTICAL_DEFAULT =  {
            {-1, 0, 1},
            {-2, 0, 2},
            {-1, 0, 1}
    };

    /**
     * default sobel horizontal mask array
     */
    protected static final int[][] SOBEL_MASK_HORIZONTAL_DEFAULT = {
            {-1, -2, -1},
            {0, 0, 0},
            {1, 2, 1}
    };

    /**
     * default mean filter mask array
     */
    protected static final int[][] MEAN_MASK = {
            {1, 1, 1},
            {1, 1, 1},
            {1, 1, 1}
    };

    /**
     * default laplacian positive mask array
     */
    public static final int[][] LAPLACIAN_POSITIVE_MASK_DEFAULT = {
            {0, 1, 0},
            {1, -4, 1},
            {0, 1, 0}
    };

    /**
     * default laplacian negative mask array
     */
    public static final int[][] LAPLACIAN_NEGATIVE_MASK_DEFAULT = {
            {0, -1, 0},
            {-1, 4, -1},
            {0, -1, 0}
    };

    public ConvMask(){

    }

    /**
     * filtered raw data of the image
     */
    protected int[][] filteredRawData;

    /**
     * apply sobel operator
     * @param rawData image raw data
     * @param vMask vertical mask
     * @param hMask horizontal mask
     * @return filtered raw data
     */
    public int[][] sobelOperater(int[][] rawData, int[][] vMask, int[][] hMask){
        //use default masks
        if(vMask == null && hMask == null){
            vMask = SOBEL_MASK_VERTICAL_DEFAULT;
            hMask = SOBEL_MASK_HORIZONTAL_DEFAULT;
        }

        //apply vertical mask and horizontal mask
        int[][] vFilteredRawData = ArrayUtils.clone2DArray(this.convolute(rawData, vMask));
        int[][] hFilteredRawData = ArrayUtils.clone2DArray(this.convolute(rawData, hMask));
        this.filteredRawData = ArrayUtils.calculate2DEdgeMagnitude(vFilteredRawData, hFilteredRawData);

        return this.filteredRawData;
    }

    /**
     * apply mean operator
     * @param rawData   image raw data
     * @param mask  mask array for mean filter
     * @return  filtered raw data
     */
    public int[][] meanOperator(int[][] rawData, int[][] mask){
        if(mask == null){
            mask = MEAN_MASK;
        }
        //apply the mask
        this.convolute(rawData, mask);

        return this.filteredRawData;
    }

    /**
     * apply laplacian operator
     * @param rawData   image raw data
     * @param mask  laplacian mask array
     * @return  filtered raw data
     */
    public int[][] laplacianOperator(int[][] rawData, int[][] mask){
        //init filtered raw data which represents the image after applying the mask
        this.filteredRawData= ArrayUtils.clone2DArray(rawData);

        if(mask == null){
            mask = LAPLACIAN_POSITIVE_MASK_DEFAULT;
        }
        //apply the mask
        this.convolute(rawData, mask);
        return this.filteredRawData;
    }

    /**
     * convolute an image
     * @param rawData   image raw data
     * @param mask      mask
     * @return  filtered raw data
     */
    public int[][] convolute(int[][] rawData, int[][] mask){
        //init filtered raw data which represents the image after applying the mask
        this.filteredRawData= ArrayUtils.clone2DArray(rawData);

        //check image length
        int imgHeight = rawData.length;
        int imgWidth = rawData[0].length;

        //check mask size - both width and height
        int maskSize = mask.length;

        //apply the mask both horizontal and vertical
        this.applyMask(rawData, mask, imgHeight, imgWidth, maskSize);

        return this.filteredRawData;
    }

    /**
     * apply a mask to the image data
     * @param rawData image data array
     * @param mask  mask array
     * @param imgHeight image height
     * @param imgWidth  image width
     * @param maskSize  mask size
     */
    protected void applyMask(int[][] rawData, int[][] mask, int imgHeight, int imgWidth, int maskSize){
        // apply horizontal mask
        for(int row=0; row < (imgHeight - maskSize); row++){
            for(int col=0; col < (imgWidth - maskSize); col++){
                int[][] targetSelection = ArrayUtils.slice2DArray(rawData, col, maskSize, row, maskSize);
                int sumDotProduct = ArrayUtils.docProduct(targetSelection, mask);
                int pixVal = sumDotProduct / (maskSize * maskSize);
                this.filteredRawData[row+(maskSize - 1)/2][col+(maskSize - 1)/2] = pixVal;
            }
        }
    }
}
