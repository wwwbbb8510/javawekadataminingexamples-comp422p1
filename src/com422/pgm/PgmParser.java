package com422.pgm;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * parse pgm file
 */
public class PgmParser {
    private static final String nl = System.getProperty("line.separator");

    protected String inputFilePath;
    protected InputStream fileInputStream;
    protected String pmType;
    protected int imgWidth;
    protected int imgHeight;
    protected int maxVal;
    protected int[][] rawData;

    /**
     * parse image data from image file
     * @param inputFilePath
     */
    public PgmParser(String inputFilePath) {
        this.inputFilePath = inputFilePath;
        this.parseToRaw();
    }

    /**
     * get image raw data
     * @return
     */
    public int[][] getRawData(){
        return this.rawData;
    }

    /**
     * output image data to file
     * @param rawData
     * @param outputPath
     */
    public void outputPgm(int[][] rawData, String outputPath){
        int outputImgHeight = rawData.length;
        int outputImgWidth = rawData[0].length;
        try {
            System.out.println("Outputting image to: " + outputPath);
            OutputStream os = new FileOutputStream(outputPath);
            DataOutputStream dos = new DataOutputStream(os);
            dos.write(this.pmType.getBytes(StandardCharsets.US_ASCII));
            dos.writeBytes(nl);
            dos.write(String.valueOf(outputImgWidth).getBytes(StandardCharsets.US_ASCII));
            dos.writeBytes(" ");
            dos.write(String.valueOf(outputImgHeight).getBytes(StandardCharsets.US_ASCII));
            dos.writeBytes(nl);
            dos.write(String.valueOf(this.maxVal).getBytes(StandardCharsets.US_ASCII));
            dos.writeBytes(nl);
            for(int i=0; i<rawData.length; i++){
                for (int j=0; j<rawData[i].length; j++){
                    dos.writeByte(rawData[i][j]);
                }
            }
            dos.close();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * parse each row of the image data
     */
    protected void parseToRaw(){
        try {
            this.fileInputStream = new FileInputStream(this.inputFilePath);
            Scanner sc = new Scanner(fileInputStream);
            this.pmType = sc.nextLine();
            this.imgWidth = sc.nextInt();
            this.imgHeight = sc.nextInt();
            this.maxVal = sc.nextInt();
            this.fileInputStream.close();
            // Now parse the file as binary data
            this.fileInputStream = new FileInputStream(this.inputFilePath);
            DataInputStream dis = new DataInputStream(fileInputStream);

            // look for 3 lines (i.e.: the header) and discard them
            int numNewLines = 3;
            while (numNewLines > 0) {
                char c;
                do {
                    c = (char)(dis.readUnsignedByte());
                } while (c != '\n');
                numNewLines--;
            }

            // read the image data
            this.rawData = new int[this.imgHeight][this.imgWidth];
            for (int row = 0; row < this.imgHeight; row++) {
                for (int col = 0; col < this.imgWidth; col++) {
                    this.rawData[row][col] = dis.readUnsignedByte();
                }
            }
            dis.close();
            this.fileInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
