package com422.utils;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayUtils {
    /**
     * slice 2d int array
     * @param src original array
     * @param startXIndex start X index
     * @param xLength   X length
     * @param startYIndex   start Y index
     * @param yLength   Y length
     * @return  2d int array
     */
    public static int[][] slice2DArray(int[][] src, int startXIndex, int xLength, int startYIndex, int yLength){
        int[][] slicedArray = new int[yLength][xLength];
        for (int row=startYIndex; row < startYIndex + yLength; row++){
            for(int col=startXIndex; col < startXIndex + xLength; col++){
                slicedArray[row-startYIndex][col-startXIndex] = src[row][col];
            }
        }
        return slicedArray;
    }

    /**
     * convert 2D array to 1D row by row
     * @param src 2 d array
     * @return
     */
    public static int[] convert2DTo1D(int[][] src){
        int[] convertedArray = new int[src.length * src[0].length];
        for(int row=0; row<src.length; row++){
            for (int col=0; col<src[0].length; col++){
                convertedArray[row * src[0].length + col] = src[row][col];
            }
        }
        return convertedArray;
    }

    /**
     * calculate the median value
     * @param target target int array
     * @return median value
     */
    public static int calculateMedian(int[] target){
        int median = 0;
        Arrays.sort(target);
        int middle = ((target.length) / 2);
        if(target.length % 2 == 0){
            int medianA = target[middle];
            int medianB = target[middle-1];
            median = (medianA + medianB) / 2;
        } else{
            median = target[middle + 1];
        }
        return median;
    }

    /**
     * clone 2D array
     * @param src original 2D array
     * @return  cloned 2D array
     */
    public static int[][] clone2DArray(int[][] src){
        int[][] des = new int[src.length][src[0].length];
        for(int row=0; row<src.length; row++){
            for (int col=0; col<src[0].length; col++){
                des[row][col] = src[row][col];
            }
        }
        return des;
    }

    /**
     * clone 2D array
     * @param src original 2D array
     * @return  cloned 2D array
     */
    public static ArrayList<ArrayList<Float>> clone2DArrayList(ArrayList<ArrayList<Float>> src){
        ArrayList<ArrayList<Float>> des = new ArrayList<>();
        for(int row=0; row<src.size(); row++){
            for (int col=0; col<src.get(0).size(); col++){
                try{
                    des.get(row).add(col, src.get(row).get(col));
                }catch (IndexOutOfBoundsException e){
                    ArrayList<Float> rowArr = new ArrayList<>();
                    des.add(rowArr);
                    des.get(row).add(col, src.get(row).get(col));
                }
            }
        }
        return des;
    }

    /**
     * slice 2D Array
     * @param src   original ArrayList
     * @param startRowIndex
     * @param startColIndex
     * @param rowSize
     * @param colSize
     * @return
     */
    public static ArrayList<ArrayList<Float>> slice2DArrayList(ArrayList<ArrayList<Float>> src, int startRowIndex, int startColIndex, int rowSize, int colSize){
        ArrayList<ArrayList<Float>> des = new ArrayList<>();
        for(int row=startRowIndex; row<startRowIndex+rowSize; row++){
            ArrayList<Float> copiedRow = new ArrayList<>();
            for (int col=startColIndex; col<startColIndex+colSize; col++){
                copiedRow.add(src.get(row).get(col));
            }
            des.add(copiedRow);
        }
        return des;
    }

    /**
     * calculate dot product of two int array
     * @param array1 first int array
     * @param array2 second int array
     * @return
     */
    public static int docProduct(int[][] array1, int[][] array2){
        int sum = 0;
        for (int row=0; row < array1.length; row ++){
            for (int col=0; col<array1[0].length; col++){
                sum += array1[row][col] * array2[row][col];
            }
        }
        return sum;
    }

    /**
     * subtract for two 2D arrays
     * @param fromArray the array that subtract from
     * @param minusArray the array that subtract
     * @return  subtracted result
     */
    public static int[][] subtract(int[][] fromArray, int[][] minusArray){
        int[][] subtractResult = new int[fromArray.length][fromArray[0].length];
        for(int row=0; row < fromArray.length; row++){
            for (int col=0; col < fromArray[0].length; col++){
                subtractResult[row][col] = fromArray[row][col] - minusArray[row][col];
            }
        }
        return subtractResult;
    }

    /**
     * addition for two 2D arrays
     * @param fromArray the array that subtract from
     * @param minusArray the array that subtract
     * @return  subtracted result
     */
    public static int[][] add(int[][] fromArray, int[][] minusArray){
        int[][] addResult = new int[fromArray.length][fromArray[0].length];
        for(int row=0; row < fromArray.length; row++){
            for (int col=0; col < fromArray[0].length; col++){
                addResult[row][col] = fromArray[row][col] + minusArray[row][col];
            }
        }
        return addResult;
    }

    /**
     * calulate the edge magnitude
     * @param vFilteredRawData  vertical filter result raw data
     * @param hFilteredRawData  horizontal filter result raw data
     * @return  edge magnitude
     */
    public static int[][] calculate2DEdgeMagnitude(int[][] vFilteredRawData, int[][] hFilteredRawData){
        int[][] magnitudeRawData = new int[vFilteredRawData.length][vFilteredRawData[0].length];
        for(int row=1; row < vFilteredRawData.length; row++){
            for(int col=1; col < vFilteredRawData[0].length; col++){
                magnitudeRawData[row][col] = (int)Math.sqrt(vFilteredRawData[row][col]*vFilteredRawData[row][col] +
                        hFilteredRawData[row][col] * hFilteredRawData[row][col]);
            }
        }
        return magnitudeRawData;
    }

    /**
     * join two Array Lists
     * @param firstArray    first array list
     * @param secondArray   second array list
     * @return
     */
    public static ArrayList<ArrayList<Object>> join2DArrayList(ArrayList<ArrayList<Object>> firstArray, ArrayList<ArrayList<Object>> secondArray){
        ArrayList<ArrayList<Object>> joinedArrayList = new ArrayList<>();
        for(int i=0; i<firstArray.size(); i++){
            ArrayList<Object> cloneRowArray =  firstArray.get(i);
            cloneRowArray.addAll(secondArray.get(i));
            joinedArrayList.add(cloneRowArray);
        }
        return joinedArrayList;
    }

    /**
     * generate a constant vector with given value and size
     * @param value element value
     * @param size  vector size
     * @return
     */
    public static ArrayList<Object> generateConstantVector(Float value, int size){
        ArrayList<Object> vectorArray = new ArrayList<>();
        for (int i=0; i<size; i++){
            vectorArray.add(value);
        }
        return vectorArray;
    }

    /**
     * append a constant value to each row of 2D array
     * @param arrData
     * @param value
     * @return
     */
    public static ArrayList<ArrayList<Object>> appendConstToArrayList(ArrayList<ArrayList<Object>> arrData, Object value){
        for (ArrayList<Object> row: arrData){
            row.add(value);
        }
        return arrData;
    }

    /**
     * calculate mean value
     * @param arrData   2D ArrayList
     * @return
     */
    public static Float calculateMean(ArrayList<ArrayList<Float>> arrData){
        Float meanValue = new Float(0);
        Float sumValue = new Float(0);
        for (ArrayList<Float> row: arrData){
            for (Float colValue: row){
                sumValue += colValue;
            }
        }
        meanValue = sumValue / (arrData.size() * arrData.get(0).size());
        return meanValue;
    }

    /**
     * calculate standard deviation
     * @param arrData   2D ArrayList
     * @param meanValue mean value
     * @return
     */
    public static Float calculateStandardDeviation(ArrayList<ArrayList<Float>> arrData, Float meanValue){
        Float deviationValue = new Float(0);
        Float sumValue = new Float(0);
        for (ArrayList<Float> row: arrData){
            for (Float colValue: row){
                sumValue += (colValue-meanValue) * (colValue-meanValue);
            }
        }
        deviationValue = sumValue / (arrData.size() * arrData.get(0).size());
        return deviationValue;
    }

    /**
     * 2D ArrayList transpose
     * @param arrData   original array
     * @return
     */
    public static ArrayList<ArrayList<Float>> transpose2DArray(ArrayList<ArrayList<Float>> arrData){
        ArrayList<ArrayList<Float>> transposedArray = new ArrayList<>(arrData.get(0).size());
        for (int row=0; row<arrData.size(); row++){
            for (int col=0; col<arrData.get(0).size(); col++){
                try {
                    transposedArray.get(col).add(arrData.get(row).get(col));
                }catch (IndexOutOfBoundsException e){
                    ArrayList<Float> rowArr = new ArrayList<>();
                    transposedArray.add(rowArr);
                    transposedArray.get(col).add(arrData.get(row).get(col));
                }
            }
        }
        return transposedArray;
    }

    /**
     * output the shape of 2D ArrayList for debug
     * @param arrData
     */
    public static void outputShapeOf2DArrayList(ArrayList<ArrayList<Object>> arrData){
        System.out.println(arrData.size());
        ArrayList firstRow = (ArrayList)arrData.get(0);
        System.out.println(firstRow.size());
    }
}
