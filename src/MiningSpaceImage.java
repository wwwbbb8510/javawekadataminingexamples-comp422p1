import com422.conv.ConvMask;
import com422.filter.SpatialFilters;
import com422.pgm.PgmParser;
import com422.segment.Thresholding;

public class MiningSpaceImage {
    /**
     * input file path
     */
    private static final String INPUT_FILE = "project1-images/2.1/hubble.pgm";
    /**
     * output file path after mean filter
     */
    private static final String OUTPUT_FILE_MEAN = "project1-images/2.1/hubble-output-mean.pgm";
    /**
     * output file path after thresholding
     */
    private static final String OUTPUT_FILE_THRESHOLDING = "project1-images/2.1/hubble-output-threshold.pgm";
    private static final String OUTPUT_FILE_THRESHOLDING_WITHOUT_MEAN = "project1-images/2.1/hubble-output-threshold-wm.pgm";

    public static void main(String[] args) {
        String inputFile = INPUT_FILE;
        String outputFileMean = OUTPUT_FILE_MEAN;
        String outputFileThresholding = OUTPUT_FILE_THRESHOLDING;
        String outputFileThresholdingWOMean = OUTPUT_FILE_THRESHOLDING_WITHOUT_MEAN;
        if(args.length == 1){
            String resourceFolder = args[0];
            inputFile = resourceFolder + inputFile;
            outputFileMean = resourceFolder + outputFileMean;
            outputFileThresholding = resourceFolder + outputFileThresholding;
            outputFileThresholdingWOMean = resourceFolder + outputFileThresholdingWOMean;
        }

        //parse the image to raw data
        PgmParser pp = new PgmParser(inputFile);
        int[][] rawData = pp.getRawData();

        //perform thresholding
        int[][] segementedRawData = Thresholding.binaryThreshold(rawData, 128,0);
        pp.outputPgm(segementedRawData, outputFileThresholdingWOMean);

        //perform median filter
        ConvMask cm = new ConvMask();
        int windowSize = 10;
        int[][] mask = new int[windowSize][windowSize];
        for (int i=0; i<windowSize; i++){
            for (int j=0; j<windowSize; j++){
                mask[i][j] = 1;
            }
        }
        int[][] filteredMean =  cm.meanOperator(rawData, mask);
        pp.outputPgm(filteredMean, outputFileMean);

        //perform thresholding
        segementedRawData = Thresholding.binaryThreshold(filteredMean, 128,0);
        pp.outputPgm(segementedRawData, outputFileThresholding);
    }
}
