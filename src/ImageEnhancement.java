import com422.conv.ConvMask;
import com422.pgm.PgmParser;
import com422.utils.ArrayUtils;

public class ImageEnhancement {
    /**
     * input file path
     */
    private static final String INPUT_FILE = "project1-images/1.3/blurry-moon.pgm";
    /**
     * output file path
     */
    private static final String OUTPUT_FILE_LAP_POSITIVE = "project1-images/1.3/blurry-moon-output-p.pgm";
    private static final String OUTPUT_FILE_EDGE_POSITIVE = "project1-images/1.3/blurry-moon-output-edge-p.pgm";
    /**
     * output file path
     */
    private static final String OUTPUT_FILE_LAP_NEGATIVE = "project1-images/1.3/blurry-moon-output-n.pgm";
    private static final String OUTPUT_FILE_EDGE_NEGATIVE = "project1-images/1.3/blurry-moon-output-edge-n.pgm";

    public static void main(String[] args) {
        String inputFile = INPUT_FILE;
        String outputFileLapPositive = OUTPUT_FILE_LAP_POSITIVE;
        String outputFileLapNegative = OUTPUT_FILE_LAP_NEGATIVE;
        if(args.length == 1){
            String resourceFolder = args[0];
            inputFile = resourceFolder + inputFile;
            outputFileLapPositive = resourceFolder + outputFileLapPositive;
            outputFileLapNegative = resourceFolder + outputFileLapNegative;
        }
        PgmParser pp = new PgmParser(inputFile);
        int[][] rawData = pp.getRawData();
        //perform positive laplacian enhancement
        ConvMask cm = new ConvMask();
        int[][] filteredLaplacian =  cm.laplacianOperator(rawData, null);
        int[][] enhancedImageRawData = ArrayUtils.subtract(rawData, filteredLaplacian);
        pp.outputPgm(enhancedImageRawData, outputFileLapPositive);
        pp.outputPgm(filteredLaplacian, OUTPUT_FILE_EDGE_POSITIVE);

        //perform negative laplacian enhancement
        filteredLaplacian =  cm.laplacianOperator(rawData, ConvMask.LAPLACIAN_NEGATIVE_MASK_DEFAULT);
        enhancedImageRawData = ArrayUtils.add(rawData, filteredLaplacian);
        pp.outputPgm(enhancedImageRawData, outputFileLapNegative);
        pp.outputPgm(filteredLaplacian, OUTPUT_FILE_EDGE_NEGATIVE);

    }
}
