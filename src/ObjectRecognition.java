import com422.data.Import;
import com422.dm.WekaDataTransfer;
import com422.utils.ArrayUtils;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class ObjectRecognition {
    /**
     * file paths for the six set of features
     */
    private final static String FILE_PATH_MFEAT_FAC = "project1-images/3.1/mfeat-digits/mfeat-fac";
    private final static String FILE_PATH_MFEAT_FOU = "project1-images/3.1/mfeat-digits/mfeat-fou";
    private final static String FILE_PATH_MFEAT_KAR = "project1-images/3.1/mfeat-digits/mfeat-kar";
    private final static String FILE_PATH_MFEAT_MOR = "project1-images/3.1/mfeat-digits/mfeat-mor";
    private final static String FILE_PATH_MFEAT_PIX = "project1-images/3.1/mfeat-digits/mfeat-pix";
    private final static String FILE_PATH_MFEAT_ZER = "project1-images/3.1/mfeat-digits/mfeat-zer";

    public static void main(String[] args) {
        String filePathMfeatFac =  FILE_PATH_MFEAT_FAC;
        String filePathMfeatFou =  FILE_PATH_MFEAT_FOU;
        String filePathMfeatKar =  FILE_PATH_MFEAT_KAR;
        String filePathMfeatMor =  FILE_PATH_MFEAT_MOR;
        String filePathMfeatPix =  FILE_PATH_MFEAT_PIX;
        String filePathMfeatZer =  FILE_PATH_MFEAT_ZER;
        if(args.length == 1){
            String resourceFolder = args[0];
            filePathMfeatFac = resourceFolder + filePathMfeatFac;
            filePathMfeatFou = resourceFolder + filePathMfeatFou;
            filePathMfeatKar = resourceFolder + filePathMfeatKar;
            filePathMfeatMor = resourceFolder + filePathMfeatMor;
            filePathMfeatPix = resourceFolder + filePathMfeatPix;
            filePathMfeatZer = resourceFolder + filePathMfeatZer;
        }

        //load the extracted features
        ArrayList loadedFac = Import.LoadFromFile(filePathMfeatFac);
        ArrayList loadedFou = Import.LoadFromFile(filePathMfeatFou);
        ArrayList loadedKar = Import.LoadFromFile(filePathMfeatKar);
        ArrayList loadedMor = Import.LoadFromFile(filePathMfeatMor);
        ArrayList loadedPix = Import.LoadFromFile(filePathMfeatPix);
        ArrayList loadedZer = Import.LoadFromFile(filePathMfeatZer);
        ArrayUtils.outputShapeOf2DArrayList(loadedFac);

        //create attribute names
        ArrayList<String> attributeNames = new ArrayList<>();
        attributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("fac", 216));
        attributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("fou", 76));
        attributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("kar", 64));
        attributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("mor", 6));
        attributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("pix", 240));
        attributeNames.addAll(WekaDataTransfer.generateSequentialAttributeNames("zer", 47));
        attributeNames.add("class");

        //combine the loaded array lists to one
        ArrayList<ArrayList<Object>> datasetX = ArrayUtils.join2DArrayList(
            ArrayUtils.join2DArrayList(
                ArrayUtils.join2DArrayList(
                    ArrayUtils.join2DArrayList(
                            ArrayUtils.join2DArrayList(loadedFac, loadedFou), loadedKar
                    ), loadedMor
                ), loadedPix
            ), loadedZer
        );
        ArrayUtils.outputShapeOf2DArrayList(datasetX);

        //generate target classes
        ArrayList<ArrayList<Object>> datasetY = new ArrayList<ArrayList<Object>>();
        for (int i=0; i<10; i++){
            ArrayList<Object> generatedClasses = ArrayUtils.generateConstantVector(new Float(i), 200);
            for (Object classValue : generatedClasses){
                ArrayList<Object> rowClass = new ArrayList<>();
                rowClass.add(classValue);
                datasetY.add(rowClass);
            }
        }

        //combine dataset X and y
        ArrayList<ArrayList<Object>> fullDataset = ArrayUtils.join2DArrayList(datasetX, datasetY);

        //convert ArrayList dataset to weka instances
        ArrayList<String> labels = new ArrayList<>();
        for (int i=0; i<10; i++){
            labels.add(String.valueOf(i));
        }
        Instances wekaFullDataset = WekaDataTransfer.createNumericDataset("face_images", attributeNames, fullDataset, labels);

        //split the dataset to training set and test set by halves
        Map<String, Instances> splitedWekaDataset = WekaDataTransfer.splitDatasetToTrainTest(wekaFullDataset, 50, 10);
        Instances trainingSet = splitedWekaDataset.get("train");
        Instances testSet = splitedWekaDataset.get("test");

        //train classifier using all features
        Classifier cls = new J48();
        try {
            trainingSet.setClassIndex(trainingSet.numAttributes() - 1);
            testSet.setClassIndex(testSet.numAttributes() - 1);
            cls.buildClassifier(trainingSet);
            Evaluation eval = new Evaluation(trainingSet);
            eval.evaluateModel(cls, testSet);
            System.out.print(eval.toSummaryString());
            System.out.print(eval.toClassDetailsString());
            double[][] confusionMatrix = eval.confusionMatrix();
            for(int i=0; i<confusionMatrix.length; i++){
                System.out.println(Arrays.toString(confusionMatrix[i]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //visualize tree
        try {
            WekaDataTransfer.visualizeJ48Tree((J48) cls, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //training classification with morphological features
        attributeNames = WekaDataTransfer.generateSequentialAttributeNames("mor", 6);
        attributeNames.add("class");
        datasetX = loadedMor;
        fullDataset = ArrayUtils.join2DArrayList(datasetX, datasetY);
        //convert ArrayList dataset to weka instances
        labels = new ArrayList<>();
        for (int i=0; i<10; i++){
            labels.add(String.valueOf(i));
        }
        wekaFullDataset = WekaDataTransfer.createNumericDataset("face_images_morphological", attributeNames, fullDataset, labels);

        //split the dataset to training set and test set by halves
        splitedWekaDataset = WekaDataTransfer.splitDatasetToTrainTest(wekaFullDataset, 50, 20);
        trainingSet = splitedWekaDataset.get("train");
        testSet = splitedWekaDataset.get("test");

        //train classifier using all features
        cls = new J48();
        try {
            trainingSet.setClassIndex(trainingSet.numAttributes() - 1);
            testSet.setClassIndex(testSet.numAttributes() - 1);
            cls.buildClassifier(trainingSet);
            Evaluation eval = new Evaluation(trainingSet);
            eval.evaluateModel(cls, testSet);
            System.out.print(eval.toSummaryString());
            System.out.print(eval.toClassDetailsString());
            double[][] confusionMatrix = eval.confusionMatrix();
            for(int i=0; i<confusionMatrix.length; i++){
                System.out.println(Arrays.toString(confusionMatrix[i]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*//visualize tree
        try {
            WekaDataTransfer.visualizeJ48Tree((J48) cls, null);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }
}
