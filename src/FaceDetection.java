import com422.data.Import;
import com422.dm.FeatureExtraction;
import com422.dm.WekaDataTransfer;
import com422.utils.ArrayUtils;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class FaceDetection {
    /**
     * file paths
     */
    private final static String FILE_DIR_TRAIN_FACE = "project1-images/2.2/mit-cbcl-faces-balanced/train/train/face";
    private final static String FILE_DIR_TRAIN_NON_FACE = "project1-images/2.2/mit-cbcl-faces-balanced/train/train/non-face";
    private final static String FILE_DIR_TEST_FACE = "project1-images/2.2/mit-cbcl-faces-balanced/test/test/face";
    private final static String FILE_DIR_TEST_NON_FACE = "project1-images/2.2/mit-cbcl-faces-balanced/test/test/non-face";
    private final static String FILE_OUTPUT_TRAINING = "project1-images/2.2/mit-cbcl-faces-balanced/train/train/training.arff";
    private final static String FILE_OUTPUT_TEST = "project1-images/2.2/mit-cbcl-faces-balanced/test/test/test.arff";

    public static void main(String[] args) {
        String fileDirTrainFace = FILE_DIR_TRAIN_FACE;
        String fileDirTrainNonFace = FILE_DIR_TRAIN_NON_FACE;
        String fileDirTestFace = FILE_DIR_TEST_FACE;
        String fileDirTestNonFace = FILE_DIR_TEST_NON_FACE;
        String fileDirOutputTraining = FILE_OUTPUT_TRAINING;
        String fileDirOutputTest = FILE_OUTPUT_TEST;
        if(args.length == 1){
            String resourceFolder = args[0];
            fileDirTrainFace = resourceFolder + fileDirTrainFace;
            fileDirTrainNonFace = resourceFolder + fileDirTrainNonFace;
            fileDirTestFace = resourceFolder + fileDirTestFace;
            fileDirTestNonFace = resourceFolder + fileDirTestNonFace;
            fileDirOutputTraining = resourceFolder + fileDirOutputTraining;
            fileDirOutputTest = resourceFolder + fileDirOutputTest;
        }
        //read data from foders
        ArrayList<ArrayList<ArrayList<Float>>> pixelTrainFaceData = Import.loadAllFromDirectory(fileDirTrainFace);
        ArrayList<ArrayList<ArrayList<Float>>> pixelTrainNonFaceData = Import.loadAllFromDirectory(fileDirTrainNonFace);
        ArrayList<ArrayList<ArrayList<Float>>> pixelTestFaceData = Import.loadAllFromDirectory(fileDirTestFace);
        ArrayList<ArrayList<ArrayList<Float>>> pixelTestNonFaceData = Import.loadAllFromDirectory(fileDirTestNonFace);

        //extract features from raw data
        Map<String, ArrayList> extractedTrainFaceData = FeatureExtraction.extractFeaturesFromPixels(pixelTrainFaceData);
        Map<String, ArrayList> extractedTrainNonFaceData = FeatureExtraction.extractFeaturesFromPixels(pixelTrainNonFaceData);
        Map<String, ArrayList> extractedTestFaceData = FeatureExtraction.extractFeaturesFromPixels(pixelTestFaceData);
        Map<String, ArrayList> extractedTestNonFaceData = FeatureExtraction.extractFeaturesFromPixels(pixelTestNonFaceData);

        //get attribute names
        ArrayList<String> attributeNames = extractedTrainFaceData.get("arributeNames");
        attributeNames.add("class"); //add class attribute name

        //create class labels
        ArrayList<String> labels = new ArrayList<>();
        labels.add("0");
        labels.add("1");

        //combine training data
        ArrayList<ArrayList<Object>> trainFaceData = extractedTrainFaceData.get("data");
        ArrayUtils.appendConstToArrayList(trainFaceData, new Integer(1));
        ArrayList<ArrayList<Object>> trainNonFaceData = extractedTrainNonFaceData.get("data");
        ArrayUtils.appendConstToArrayList(trainNonFaceData, new Integer(0));
        ArrayList<ArrayList<Object>> trainData = new ArrayList<>();
        trainData.addAll(trainFaceData);
        trainData.addAll(trainNonFaceData);

        //combine test data
        ArrayList<ArrayList<Object>> testFaceData = extractedTestFaceData.get("data");
        ArrayUtils.appendConstToArrayList(testFaceData, new Integer(1));
        ArrayList<ArrayList<Object>> testNonFaceData = extractedTestNonFaceData.get("data");
        ArrayUtils.appendConstToArrayList(testNonFaceData, new Integer(0));
        ArrayList<ArrayList<Object>> testData = new ArrayList<>();
        testData.addAll(testFaceData);
        testData.addAll(testNonFaceData);

        //create weka instances for training data
        Instances wekaTrainingData = WekaDataTransfer.createNumericDataset("face-detection-training", attributeNames, trainData, labels);

        //create weka instances for test data
        Instances wekaTestData = WekaDataTransfer.createNumericDataset("face-detection-test", attributeNames, testData, labels);

        //save weka instances to arff files
        try {
            ConverterUtils.DataSink.write(fileDirOutputTraining, wekaTrainingData);
            ConverterUtils.DataSink.write(fileDirOutputTest, wekaTestData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //train classifier
        Classifier cls = new NaiveBayes();
        wekaTrainingData.setClassIndex(wekaTrainingData.numAttributes() - 1);
        wekaTestData.setClassIndex(wekaTestData.numAttributes() - 1);
        try {
            cls.buildClassifier(wekaTrainingData);
            Evaluation eval = new Evaluation(wekaTrainingData);
            eval.evaluateModel(cls, wekaTestData);
            System.out.print(eval.toSummaryString());
            System.out.print(eval.toClassDetailsString());
            double[][] confusionMatrix = eval.confusionMatrix();
            for(int i=0; i<confusionMatrix.length; i++){
                System.out.println(Arrays.toString(confusionMatrix[i]));
            }
            WekaDataTransfer.plotROC(eval, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
