import com422.conv.ConvMask;
import com422.pgm.PgmParser;

/**
 * edge detection
 */
public class EdgeDetection {

    /**
     * input file path
     */
    private static final String INPUT_FILE = "project1-images/1.1/test-pattern.pgm";
    /**
     * output file path
     */
    private static final String OUTPUT_FILE = "project1-images/1.1/test-pattern-output.pgm";

    public static void main(String[] args) {
        String inputFile = INPUT_FILE;
        String outputFile = OUTPUT_FILE;
        if(args.length == 1){
            String resourceFolder = args[0];
            inputFile = resourceFolder + inputFile;
            outputFile = resourceFolder + outputFile;
        }

        //parse the image to raw data
        PgmParser pp = new PgmParser(inputFile);
        int[][] rawData = pp.getRawData();

        //perform sobel operator
        ConvMask cm = new ConvMask();
        int[][] filteredDataSobel =  cm.sobelOperater(rawData, null, null);
        pp.outputPgm(filteredDataSobel, outputFile);
    }
}
