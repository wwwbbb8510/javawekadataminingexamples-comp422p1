# Dependency

The whole project is developed in pure Java 8 apart from using only one data mining library - Weka 3.8 library, so please download Weka 3.8 API library is you don't have one.

For some image conversion, please also install netpbm which will be used to convert tif to pgm files.

# Get the images and data ready

Download the data zip file named project1-images.zip, put it in the project root folder and unzip the file.

For 2.2, please go to project1-images/2.2/mit-cbcl-faces-balanced folder and un-compress test.tar.gz and train.tar.gz

The directory structure should be the same as below:
Project1
    - src
    - project1-images
        - 1.1
            - test-patter.tif
        - 1.2
            - ckt-board-saltpep.tif
        - 1.3
            - blurry-moon.tif
        - 2.1
            - hubble.tif
        - 2.2
            - mit-cbcl-faces-balanced
                - test
                    - test
                        - face
                        - non-face
                - train
                    - train
                        - face
                        - non-face
        - 3.1
            - mfeat-digits
                - mfeat-fac
                - mfeat-fou
                - .........

# Import the source code to a JAVA IDE (JetBrains is recommended)

Create a J2SE project and import the whole Project1 folder and make Project1 as the project root folder.

# Add Weka library

Add weka.jar in the dependencies of the java project

# convert tif images to pgm images for the purpose of import the greysacle pixel values before running java code

Go to the project root foler - Project1

Run the following commands to convert all related images

``` convert images
tifftopnm project1-images/1.1/test-pattern.tif > project1-images/1.1/test-pattern.pgm
tifftopnm project1-images/1.2/ckt-board-saltpep.tif > project1-images/1.2/ckt-board-saltpep.pgm
tifftopnm project1-images/1.3/blurry-moon.tif > project1-images/1.3/blurry-moon.pgm
tifftopnm project1-images/2.1/hubble.tif > project1-images/2.1/hubble.pgm
```

# Running the corresponding tasks (You can choose any image viewer/editor such as gimp to view pgm image file)

## 1.1 Edge Detection

Run main method of EdgeDetection class in your IDE

project1-images/1.1/test-pattern-output.pgm will be the output image

## 1.2 Noise Cancellation

Run main method of NoiseCancellation class in your IDE

There will be two output files - project1-images/1.2/ckt-board-saltpep-output-mean.pgm for mean filter and project1-images/1.2/ckt-board-saltpep-output-median.pgm for median filter

## 1.3 Image Enhancement

Run main method of ImageEnhancement class in your IDE

There will be two output files - project1-images/1.3/blurry-moon-output-p.pgm for Laplacian positive operator and project1-images/1.3/blurry-moon-output-n.pgm for Laplacian negative operator

## 2.1 Mining Space Images

Run main method of MiningSpaceImage class in your IDE

There will be two output files - project1-images/2.1/hubble-output-threshold-wm.pgm for the map without smoothing and project1-images/2.1/hubble-output-threshold.pgm for the map with smoothing

In my report, I tweaked the size of the mean filter and the threshold which can be changed in line 41 and lien 52 in the source code.

## 2.2 Face Detection

Run main method of FaceDetection class in your IDE

All of the output can be seen within the IDE

## 3.1 Object Recognition: Classiﬁcation of Hand-Written Digits

Run main method of ObjectRecognition class in your IDE

All of the output can be seen within the IDE

# Running scripts

Please see the file SCRIPTS to check the executable commands for each task